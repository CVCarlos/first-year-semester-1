//This program prints a calendar after be given the starter day of the month, 
//the numbers of days of the month and the number of day that need to be 
//highlighted
//Made by: Carlos Andres Cerda Veloz, group W

public class CalendarHighlight
{
   public static void main(String[] args)
   {
   
   //Head of the table
   printLines();    
   printColumns(); 

   //Printing the table
   printRow(Integer.parseInt(args[0]),Integer.parseInt(args[1]),
                 Integer.parseInt(args[2]));

   //End of the table
   printLines();

   }//main
   
   //This method print the first line of the table
   private static void printLines()
   {
      
      printSpace();
      for (int index = 1; index <= 7; index++)
      {
      System.out.print("----");
      }
      System.out.println(" ");

   }//printLines

   //This method print the columns of the table
   private static void printColumns()
   {
      System.out.print("|"); 

      for (int dayNameNo = 1; dayNameNo <= 7; dayNameNo++)
      {
      printSpace();
      dayName(dayNameNo);  
      printSpace();    
      }

      System.out.println("|");         

   }//printColumns

   //Print day names 
   private static void dayName(int dayNo)
   {
      switch (dayNo)
      {
         case 1: System.out.print("Su"); break;
         case 2: System.out.print("Mo"); break;
         case 3: System.out.print("Tu"); break;
         case 4: System.out.print("We"); break;
         case 5: System.out.print("Th"); break;
         case 6: System.out.print("Fr"); break;
         case 7: System.out.print("Sa"); break;
      }
   }//dayName
   
   //Print the calendar
   private static void printRow(int firstDay, int lastDay, int Highlight)
   {
      int dayNumber = 1;

      for (int indexRow = 1; indexRow <= 6; indexRow++)
      {
      System.out.print("|");
      for (int indexDay = 1; indexDay <= 7; indexDay++)
      {             
      if (indexDay == firstDay && dayNumber <= lastDay)
      {
      printDays(dayNumber, Highlight); 
      dayNumber++;
      firstDay = indexDay + 1;               
      } 
      else
      System.out.print("    ");           
      }//for-indexDay  

      firstDay = 1;             
      System.out.println("|");
 
      }//for-indexRow    
      
   }//printRow
   
   private static void printDays(int day, int Highlight)
   {
      if (day == Highlight)
      {
      System.out.print("<");
      System.out.printf("%02d", day);
      System.out.printf(">");
      }
      else
      {
      printSpace(); 
      System.out.printf("%02d", day);
      printSpace(); 
      }
   }

   private static void printSpace()
   {
   System.out.printf(" ");
   }//printSpace

}//class


