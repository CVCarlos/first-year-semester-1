//This program accepts two polynomials, save a reference using a 
//separete class, then it use the same class to compare both polynomials
//and print if one of the polynomials is equal, less than o bigger than 
//the other 
//Made by: Carlos Andres Cerda Veloz, group W
public class CompareQuadPoly
{
   public static void main (String[] args)
   {

      QuadPoly coefficient1 = new QuadPoly (Double.parseDouble(args[0]),
                              Double.parseDouble(args[1]),
                              Double.parseDouble(args[2]));  

      QuadPoly coefficient2 = new QuadPoly (Double.parseDouble(args[3]),
                              Double.parseDouble(args[4]),
                              Double.parseDouble(args[5]));

      compareResult(coefficient1, coefficient2);

   }//main

   //This method print the results of the comparison
   private static void compareResult(QuadPoly coefficient1, 
                                      QuadPoly coefficient2)
   {  
 
   if(coefficient1.equal(coefficient2))
   {
      System.out.print("The polynomial:     ");
      polynomialPrint(coefficient1);
      System.out.print("is the same as:     ");
      polynomialPrint(coefficient2);
   }//if
   else if (coefficient1.lessThan(coefficient2))
   {
      System.out.print("The polynomial:     ");
      polynomialPrint(coefficient1);
      System.out.print("is less than:       ");
      polynomialPrint(coefficient2);
   }//else if
   else
   {
      System.out.print("The polynomial:     ");
      polynomialPrint(coefficient1);
      System.out.print("is greater than:    ");
      polynomialPrint(coefficient2);
   }//else

   }//compareResult
   
   //This method print the polynomials
   private static void polynomialPrint(QuadPoly coefficient1)
   {
      System.out.println(coefficient1.first + "x^2 + " +
                      coefficient1.second + "x + " + coefficient1.third);
   }//polynomialPirnt1

   private static void polynomialPirnt2(QuadPoly coefficient2)
   {
      System.out.println(coefficient2.first + "x^2 + " +
                      coefficient2.second + "x + " + coefficient2.third);
   }//polynomialPirnt2
 
}//class 
