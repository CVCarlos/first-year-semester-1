//This class is used to save a reference to the coefficients of the polynomials
//Made by: Carlos Andres Cerda Veloz, group W
public class QuadPoly
{
public double first, second, third; 

public QuadPoly (double firstCoefficient, double secondCoefficient,
                 double thirdCoefficient)
{
   first = firstCoefficient;
   second = secondCoefficient;
   third = thirdCoefficient;
}
}
