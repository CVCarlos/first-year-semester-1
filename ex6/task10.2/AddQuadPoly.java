//This program got two polynomials and used an object clss to create instances
//of the polynomials coefficients. 
//Then the program sum the two polynomials and print the results
//Made by: Carlos Andres Cerda Veloz, group W
public class AddQuadPoly
{
   public static void main(String[] args)
   {
      QuadPoly polynomial1 = new QuadPoly(Double.parseDouble(args[0]), 
                                              Double.parseDouble(args[1]),
                                              Double.parseDouble(args[2]));

      QuadPoly polynomial2 = new QuadPoly(Double.parseDouble(args[3]), 
                                              Double.parseDouble(args[4]),
                                              Double.parseDouble(args[5]));

      QuadPoly polynomial3 = new QuadPoly((polynomial1.first + 
                                          polynomial2.first), 
                                         (polynomial1.second + 
                                          polynomial2.second),
                                         (polynomial1.third + 
                                          polynomial2.third));

      printResults(polynomial1, polynomial2, polynomial3);
   }//main
   
   //This method print the results
   private static void printResults(QuadPoly polynomial1, QuadPoly polynomial2, 
                                     QuadPoly polynomial3)
   {
   System.out.println("Polynomial:    " + polynomial1.first + "x + " 
                      + polynomial1.second + "y + " + polynomial1.third);

   System.out.println("added to:      " + polynomial2.first + "x + " 
                      + polynomial2.second + "y + " + polynomial2.third);

   System.out.println("results in:    " + polynomial3.first + "x + "
                      + polynomial3.second + "y + " + polynomial3.third);
   }//printResults

}//class
