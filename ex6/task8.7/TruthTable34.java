//This program prints a truth table for three propositional expressions which
//are hard coded as methods p1, p2, and p3 and which are expressions involving 
//four variables
//Made by: Carlos Andres Cerda Veloz, group W

public class TruthTable34
{
   public static void main(String[] args)
   {
   printColumnLinesUp();
   printVariables();
   printColumnLinesdDown();
   
   

   //Inicitalizing each of the tree variables
   boolean a = true, b = true, c = true, d = true;
   //Running the truth evaluation
   for (int aIndex = 1; aIndex <= 2; aIndex++, a = !a)
   {
      for (int bIndex = 1; bIndex <= 2; bIndex++, b = !b)
      {
         for (int cIndex = 1; cIndex <= 2; cIndex++, c = !c)
         {
            for (int dIndex = 1; dIndex <= 2; dIndex++, d = !d)
            {
               printResult(a, b ,c ,d);             
            }//for-dIndex
         }//for-cIndex
      }//for-bIndex
   }//for-aIndex

   printColumnLinesdDown();

   }//main
   
   private static void printColumnLinesUp()
   {
      System.out.println(" ______________________________________________" +
                         "_________");
   }//printColumnlines

   private static void printVariables()
   {
      System.out.println("|   a   |   b   |   c   |   d   |   p1  |   p2  " +
                         "|   p3  |");
   }//printVariables
 
   private static void printColumnLinesdDown()
   {
      System.out.println("|_______|_______|_______|_______|_______|_______|"
                         +"_______|");
   }//printVariables

   private static boolean p1Evaluation(boolean a, boolean b, boolean c, 
                                       boolean d)
   {
      return (((a || b) && c) || ((b || c) && d)) && (a || d);
   }//p1Evaluation

      private static boolean p2Evaluation(boolean a, boolean b, boolean c, 
                                          boolean d)
   {
      return a && c || b && d || c && d;      
   }//p1Evaluation

   private static boolean p3Evaluation(boolean a, boolean b, boolean c, 
                                       boolean d)
   {
      return (b || c) && (c || d) && (a || d);
   }//p1Evaluation

   private static void printResult(boolean a, boolean b, boolean c, boolean d)
   {
   System.out.println("|" + evaluationResult(a) + "|" + evaluationResult(b) +
                      "|" + evaluationResult(c) + "|" + evaluationResult(d) +
                      "|" + evaluationResult(p1Evaluation(a, b, c, d)) + 
                      "|" + evaluationResult(p2Evaluation(a, b, c, d)) +
                      "|" + evaluationResult(p3Evaluation(a, b, c, d)) + "|");
   }//printResult
      private static String evaluationResult(boolean evaluation)
   {
      return evaluation ? "true   " : "false  ";
   }//evaluationResult
   
}//class


