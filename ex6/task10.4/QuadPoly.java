//This class is used to save a reference to the coefficients of the polynomials
//Made by: Carlos Andres Cerda Veloz, group W
public class QuadPoly
{
   public double first, second, third;

   public QuadPoly (double firstCoefficient, double secondCoefficient, 
                    double thirdCoefficient)
   {
      first = firstCoefficient;
      second = secondCoefficient;
      third = thirdCoefficient;
   }//Constructor-method

   public boolean equal (QuadPoly otherPolynomial)
   {  
      return first == otherPolynomial.first && second == otherPolynomial.second 
             && third == otherPolynomial.third;
   }//equals
   
   
   public boolean lessThan (QuadPoly otherPolynomial)
   {  
      return first < otherPolynomial.first || second < otherPolynomial.second 
             || third < otherPolynomial.third;
   }//lessThan
   
   public String toString()
   {  
      return first + "x^2 + " + second + "x + " + third;
   }//toString

}//class

