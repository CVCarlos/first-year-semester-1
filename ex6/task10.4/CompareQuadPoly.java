//This program accepts two polynomials, save a reference using a 
//separete class, then it use the same class to compare both polynomials
//and print if one of the polynomials is equal, less than o bigger than 
//the other. Same class is used to print the results
//Made by: Carlos Andres Cerda Veloz, group W
public class CompareQuadPoly
{
   public static void main (String[] args)
   {

      QuadPoly polynomial1 = new QuadPoly (Double.parseDouble(args[0]),
                              Double.parseDouble(args[1]),
                              Double.parseDouble(args[2]));  

      QuadPoly polynomial2 = new QuadPoly (Double.parseDouble(args[3]),
                              Double.parseDouble(args[4]),
                              Double.parseDouble(args[5]));

      compareResult(polynomial1, polynomial2);

   }//main

   //This method print the results of the comparison
   private static void compareResult(QuadPoly polynomial1, 
                                      QuadPoly polynomial2)
   {  
 
   if(polynomial1.equal(polynomial2))
   {
      System.out.println("The polynomial:     " +  polynomial1.toString());
      System.out.println("is the same as:     " +  polynomial2.toString());

   }//if
   else if (polynomial1.lessThan(polynomial2))
   {
      System.out.println("The polynomial:     " + polynomial1.toString());
      System.out.println("is less than:       " + polynomial2.toString());
   }//else if
   else
   {
      System.out.println("The polynomial:     " + polynomial1.toString());

      System.out.println("is greater than:    " + polynomial2.toString());

   }//else

   }//compareResult   
 
}//class 
