//This program takes to integers and prints out the first number 
//raised to the power of the second
public class Power
{
   public static void main(String[] args)
   {

     //Here the program takes both integers 
     //The accumulator helps to calculate the power

     int firstNumber = Integer.parseInt(args[0]);
     int secondNumber = Integer.parseInt(args[1]);
     int accumulator = 1;

     //Here the program calculates the power.

     for (int count = 1; count <= secondNumber; count++)
     accumulator = accumulator * firstNumber;

     System.out.println("The result of " + firstNumber + " to the power of "
                        + secondNumber + " is " + accumulator);
     }
}
     


