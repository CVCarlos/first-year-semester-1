//THis programs prints an isoceles rigth angled triangle with
//its longest row at the top and the right hand side straight
public class PrintTriangleMirror
{
   public static void main(String[] args)
   {

     //Input heigth of the triangle

     int height = Integer.parseInt(args[0]);
   
     //Printing the triangle
     
     int control = height;
     int compare = 0;

     for (int indexHeight = 1; indexHeight <= height; indexHeight++)
     {
       control = control - 1;
       compare = control;
       for (int indexWidth = 1; indexWidth <= height; indexWidth++)
         {
         compare = compare + 1;                      
         if (compare < height)
         System.out.print("   ");               
         else
         System.out.print("[_]");
         }
         System.out.println();
     }
   }
}
