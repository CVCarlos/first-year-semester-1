//This programs print a 19 times 19 labelled table indiciating
//which of all the pairs made up of inegers between 2 and 20
//have common factors bigger than one.
// -#- represents a factor bigger than one and -|- the opposite 

public class CommonFactorsTable
{
   public static void main(String[] args)
   {

     //This three variables are used to calculate the GCD (Great Common Divisor)
 
     int compareGreater = 0;
     int compareSmaller = 0;
     int greatCommonDivisor = 0;

     //Here the program print the first frames of the table

     System.out.print("|-----|");
     for (int indexColumn = 1; indexColumn < 18; indexColumn++)
     System.out.print("----");
     
     System.out.println("-|");
     System.out.print("|     |");

     //Here the program print the content of the first row: numbers from 2 to 20

     for (int indexColumnNumbers = 2; indexColumnNumbers <= 20; indexColumnNumbers++)

     //Four spaces per number

     System.out.print("  " + indexColumnNumbers);

     System.out.println(" |");

     //Third row frames

     System.out.print("|-----|");

     for (int indexColumn = 1; indexColumn < 18; indexColumn++)
     System.out.print("----");
     
     System.out.println("-|");
	
     //Here the program print the content of the table, first for for the column numbers
     //second row for row numbers
    
     for (int indexColumnNumbers = 2; indexColumnNumbers <= 20; indexColumnNumbers++)
     {
     
       //Due to table design the numbers up to 9 are printed with 5 spaces
       //from 10 to 20 are printed with 4 spaces

       if (indexColumnNumbers < 10)
       System.out.print("|   " + indexColumnNumbers + " |-");
       else 
       System.out.print("|  " + indexColumnNumbers + " |-");

       //Here the program calculates the common divisor between the column number and 
       //each row number

       for (int indexRowNumbers = 2; indexRowNumbers <= 20; indexRowNumbers++)
       {
         compareGreater = indexColumnNumbers;
         compareSmaller = indexRowNumbers;

         greatCommonDivisor = indexColumnNumbers;

         while(compareGreater != compareSmaller)
         {
           if (compareGreater > compareSmaller)
           {
             compareGreater = compareGreater - compareSmaller;
             greatCommonDivisor = compareGreater;     
           }   
           else
           {
             compareSmaller = compareSmaller - compareGreater;
             greatCommonDivisor = compareSmaller;   
           }
         }
           
            
       //Here the table print the result representation explained at the begining of the code
       //Due to table design the numbers up to 9 are printed with 3 space representation
       //from 10 to 20 are printed with 4 spaces representation

       if (indexRowNumbers < 10)
       {
         if (greatCommonDivisor > 1)
         System.out.print("-#-");
         else 
         System.out.print("-|-");       
       }
       else
       { 
         if (greatCommonDivisor > 1)
         System.out.print("--#-");
         else 
         System.out.print("--|-");  
       } 
      } 
System.out.println("|");                  
     }
   }
}
       
     
