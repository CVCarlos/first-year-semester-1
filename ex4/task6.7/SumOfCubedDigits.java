//This program works by looping through the numbers 100 to 999 in order to
//find the four numbers with the property that the sum of the three digits in the
//number is equal to the numer itself.
public class SumOfCubedDigits
{
   public static void main(String[] args)
   {
   
   //The first loop is used for first digit of the number, the second for second
   //digit and sequentially.
  
   for (int indexFirstDigit = 1; indexFirstDigit <= 9; indexFirstDigit++)
   {
     for (int indexSecondDigit = 0; indexSecondDigit <= 9; indexSecondDigit++)
     {
       for (int indexThirdDigit = 0; indexThirdDigit <= 9; indexThirdDigit++)  
       {

         //The cubes of each digit is calculated

         double FirstDigitCube = Math.pow(indexFirstDigit, 3);
         double SecondDigitCube = Math.pow(indexSecondDigit, 3);
         double ThirdDigitCube = Math.pow(indexThirdDigit, 3);

         //Here the program determines the number

         int Number = (indexFirstDigit * 100) + (indexSecondDigit*10) + indexThirdDigit;

         //THe program check if the number meet the property, if yes, prints it

         if ((FirstDigitCube + SecondDigitCube + ThirdDigitCube) == Number )
         System.out.println("The number " + Number + " meet the property");
       }
     }
   }
   }
}
