//THis programs prints a rectangle whith a hole in the center
public class PrintHoledRectangle
{
   public static void main(String[] args)
   {

     //Input heigth and width of rectangle

     int height = Integer.parseInt(args[0]);
     int width = Integer.parseInt(args[1]);
     
     //Converting values to odd numbers
     
     height = (height / 2) * 2 + 1;

     width = (width / 2) * 2 + 1;

     //Determining the middle point

     int middlePointHeight = height/2 + 1;
     int middlePointWidth = width/2 + 1;
 
     //Printing the rectangle

     for (int indexHeight = 1; indexHeight <= height; indexHeight++)
     {
       for (int indexWidth = 1; indexWidth <= width; indexWidth++)                     
         if (indexWidth == middlePointWidth && indexHeight == middlePointHeight)
         System.out.print("   ");               
         else
         System.out.print("[_]");

         System.out.println();
     }
   }
}
