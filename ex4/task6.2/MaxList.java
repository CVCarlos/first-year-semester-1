//This program takes a list of numbers and calculate and prints the maximum number
//together with its index
public class MaxList
{
   public static void main(String[] args)
   {
     
     //Here the program takes the first number

     double number = Double.parseDouble(args[0]);
   
     //Here the program takes the list of numbers

     for (int index = 1; index < 6; index++)
     
     number = Double.parseDouble(args[index]);
     
     //Here the program determines the maximum number

     double maximumNumber = 0;
     int indexCounter = 0;

     for (int index = 0; index <= 5; index++)
     {
         indexCounter = Double.parseDouble(args[index]) >= maximumNumber ? index:indexCounter;
         maximumNumber = Double.parseDouble(args[index]) >= maximumNumber ? Double.parseDouble(args[index]):maximumNumber;                
     }

     //Print out the maximum number and its index

     System.out.println("The maximum is " + maximumNumber + " with the index " + indexCounter);     
   }
}   
