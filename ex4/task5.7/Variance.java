//This program takes a list of integers and calculates the variance of this set of numbers
public class Variance 
{
   public static void main(String[] args)
   {
   
     //Here the program takes the integers and calculate and print out its mean
     
     int sumOfNumbers = Integer.parseInt(args[0]);
     int firstNumber = sumOfNumbers;
    
     for (int index = 1; index < 5; index++)
     
     sumOfNumbers = sumOfNumbers + Integer.parseInt(args[index]);
     
     double mean = sumOfNumbers / (double)5;

     System.out.println("The mean average is " + mean);
    
     
     // Here the program calculate the sum of the square of the deviations.

     double deviation = firstNumber - mean;
     double squaresSum = Math.pow(deviation,2);
     
     for (int index = 1; index < 5; index ++)
     {
       deviation = Integer.parseInt(args[index]) - mean;     
       squaresSum = squaresSum + Math.pow(deviation,2);
     }

     //Here the program calculate the variance and print the result

     System.out.println("The variance is " + squaresSum / (double)5);
    
     }
}
 
     
     
      
     

