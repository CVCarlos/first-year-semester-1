//This program calculates and print the sin of a list of numbers 
//The start, increment and end of the list is given by command line arguments
public class SinTable
{
   public static void main(String[] args)
   {    
    
     int startingPoint = Integer.parseInt(args[0]);
     
     int increment = Integer.parseInt(args[1]);

     int endingPoint = Integer.parseInt(args[2]);

//Here the program print the table's title

     System.out.println("---------------------------------------------");
     System.out.println("| Sin table from " + startingPoint + " to " + endingPoint + " in steps of " + increment);  
     System.out.println("---------------------------------------------");

//Here the program calculates and print the table

     for (int index = startingPoint; index <= endingPoint; index = index + increment)
     
     System.out.println("| sin(" + index + ") = " + Math.sin(Math.toRadians((double)index)));

   }
}
