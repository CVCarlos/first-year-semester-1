//This program takes three ages and determines the Great Common Divisor (GCD) of the three
//Then this return in how many slices a cake need to be divided so each of the tree ages
//get an proportional number of slices
public class DivideCake3
{
   public static void main(String[] args)
   {

     //Input of thee ages 

     int firstAge = Integer.parseInt(args[0]);
     int secondAge = Integer.parseInt(args[1]);
     int thirdAge = Integer.parseInt(args[2]);

     //The Compare variables are used to save the results
     //when calculating the GCD 
     
     int compareGreater = firstAge;
     int compareSmaller = secondAge;

     //Here, if the result of compare the two first ages is equal the GCD get the value of
     //whatever of the two ages. This value is temporal.

     int temporalgreatCommonDivisor = firstAge;

     //Here the program start calculating the GCD of the first ages

     while(compareGreater != compareSmaller)
     {
       if (compareGreater > compareSmaller)
       {
         compareGreater = compareGreater - compareSmaller;
         temporalgreatCommonDivisor = compareGreater;     
       }   
       else
       {
         compareSmaller = compareSmaller - compareGreater;
         temporalgreatCommonDivisor = compareSmaller;     
       }
     }

     //Here, if the result of compare the third age and the temporal GCD is equal the GCD get 
     //the value of whatever of the two values. This value is temporal.

     int greatCommonDivisor = thirdAge;

     compareGreater = temporalgreatCommonDivisor;
     compareSmaller = thirdAge;

     //Here the program start calculating the final GCD

     while(compareGreater != compareSmaller)
     {
       if (compareGreater > compareSmaller)
       {
         compareGreater = compareGreater - compareSmaller;
         greatCommonDivisor = compareGreater;     
       }   
       else
       {
         compareSmaller = compareSmaller - compareGreater;
         greatCommonDivisor = compareSmaller;     
       }
     }

     //Here the program calculate into how much slices the cake need be divided.
     //Hence, the number of slices corresponding to each age.

     int portionFirstAge = firstAge / greatCommonDivisor;
     int portionSecondAge = secondAge / greatCommonDivisor;
     int portionThirdAge = thirdAge / greatCommonDivisor;

     int cakeDivisions = portionFirstAge + portionSecondAge + portionThirdAge;

     //Print out of the results

     System.out.println("The GCD of " + firstAge + ", " + secondAge + " and " + thirdAge + " is " + greatCommonDivisor);
     System.out.println("The cake should be divided into " + cakeDivisions);
     System.out.println("The " + firstAge + " years old gets " + portionFirstAge + " , the " + secondAge + " years old gets " 
                        + portionSecondAge + " and the " + thirdAge + " years old gets " + portionThirdAge);    
     
   }
}
     
     
