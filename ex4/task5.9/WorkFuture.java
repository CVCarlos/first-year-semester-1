//This program takes the present year and birthday year of a user and calculates
//and print his future working time before retirement at 68 years
public class WorkFuture 
{
   public static void main(String[] args)
   {
   
//Here the program takes the present and birthday year
 
     int PresentYear = Integer.parseInt(args[0]);
     int BirthdayYear = Integer.parseInt(args[1]);

//Calculate and print how many years left to work
 
     int YearsLeft = 68 - (PresentYear - BirthdayYear);
     System.out.println("You have " + YearsLeft + " years left to work");

//Calculate and print the working time

     for (int index = YearsLeft-1; index > 0; index--)
     {
       PresentYear = PresentYear + 1;
       System.out.println("In " + PresentYear + " you will have " + index + " years left to work");
     }
     
//Calculate and print retirement year

     System.out.println("You will retire in " + (PresentYear + 1));
   }
}

