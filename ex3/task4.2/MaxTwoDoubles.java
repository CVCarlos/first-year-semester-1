//This program takes two comman line arguments, interprets them as doubles values and report 
//both numbers and which of them is the greatest value.
public class MaxTwoDoubles
{
   public static void main(String[] args)
   {
//Here both values are taken and interpreted as double values

     double FirstNumber = Double.parseDouble(args[0]);
     double SecondNumber = Double.parseDouble(args[1]);

//Report both values

     System.out.println("The values are " + args[0] + " and " + args[1] );
    
//Here the program determine which is the maximum and report which is
//the greatest
 
     if (FirstNumber > SecondNumber)
       System.out.println("The greatest value is " + args[0]);
     else if (FirstNumber < SecondNumber)
       System.out.println("The greatest value is " + args[1]);
     else
       System.out.println("Both values are equal");   
   }
}
