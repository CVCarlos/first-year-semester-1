//The program determines the largest square number which is equal or smaller
//than a comman line given number
public class LargestSquare
{
   public static void main(String[] args)
   {

//Here the program takes the number 
//The value of the starter square number is equal to the given number

     int Number = Integer.parseInt(args[0]);
     int SquareNumber = Number;

//Here the program determines the largest square
 
     while (Math.pow(SquareNumber,2) > Number)
       SquareNumber = SquareNumber - 1;

     System.out.println("The largest square number smaller or equal to " + args[0] +
                        " is " + SquareNumber);
   }
}
  
