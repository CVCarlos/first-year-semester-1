//This program takes a postgraduate student mark and reports
//wheter it is a pass or fail and then, if possibly, a distinction
public class PassFailDistinction
{
   public static void main(String[] args)
   {

// Here the program takes the student mark

     double StudentMark = Double.parseDouble(args[0]);

//Here the program determines wheter the mark is a pass and or not a distinction,
//or a fail

     if (StudentMark >= 70)
     {
       System.out.println("The mark is a Pass");
       System.out.println("And the mark is a Distinction");
     }
     else if (StudentMark >= 50)
       System.out.println("The mark is a Pass");    
     else
       System.out.println("The mark is a Fail"); 
   }
}
