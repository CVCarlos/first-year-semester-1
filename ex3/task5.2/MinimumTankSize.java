//The program determine the minimum side length needed to hold 
//a command line given volume
public class MinimumTankSize
{
   public static void main(String[] args)
   {
     
//Here the program takes the volume required

     double RequiredVolume = Double.parseDouble(args[0]);
     double SideLength = 0.1;

//Here the program calculates the minimum side length required

     while (SideLength*SideLength*SideLength < RequiredVolume)
       SideLength = SideLength + 0.1;

     System.out.println("It is necessary a tank of " + SideLength + 
                        " metres of side length to hold the required volume "
                        + args[0]);
   }
}
     
