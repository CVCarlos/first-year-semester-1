//The program takes a a number of variables and calculates the number of bits 
//necessary to represent the given number of variables. 
public class MinimumBitWidth
{
   public static void main(String[] args)
   {

//Here the program takes the number of variables
//The number of bits is defined in 1
//The bit counter is defined in 0

     int NumberOfVariables = Integer.parseInt(args[0]);
     int BitNumber = 1;
     int BitCount  = 0;
   
//Here the program determines the number of bits necessary

     while (BitNumber < NumberOfVariables)
     {
       BitNumber = BitNumber * 2;
       BitCount = BitCount + 1;
     }
     System.out.println("The required number of bits to represent " + args[0] +
                        " is " + BitCount);
   }
}
   
