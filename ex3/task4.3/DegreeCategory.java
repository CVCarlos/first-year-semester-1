//This program takes a student mark and reprots what degree
//category it is worth
public class DegreeCategory
{
   public static void main(String[] args)
   {
//Here the program takes the Student Mark

     double StudentMark = Double.parseDouble(args[0]);

//Here the program determines what degree the mark is worth
//and report it

     if (StudentMark >= 70)
       System.out.println("The degree is Honours, first class");
     else if (StudentMark >= 60)
       System.out.println("The degree is Honours, second class, division one");
     else if (StudentMark >= 50)
       System.out.println("The degree is Honours, second class, division two");
     else if (StudentMark >= 40)
       System.out.println("The degree is Honours, third class");
     else if (StudentMark >= 32)
       System.out.println("The degree is Pass, ordinary degree");
     else
       System.out.println("The person Fail the course");
   }
}




        
