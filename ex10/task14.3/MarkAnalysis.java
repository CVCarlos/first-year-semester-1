/**
  This program takes a list of sudents courseworks marks and produces a report

  @author Carlos Cerda Veloz
*/

import java.util.Scanner;

public class MarkAnalysis
{
  public static void main(String[] args)
  {
     Scanner inputScanner = new Scanner(System.in);

     //An scanner to get the marks
     System.out.print("Input the number of marks: ");
     int markNumber = inputScanner.nextInt();
     double[] markValue = new double[markNumber];

     //This for is for read the mark of each student
     for(int index = 0; index < markValue.length; index++)
     {
        System.out.printf("Enter mark # %2d: ", (index+1));
        markValue[index] = inputScanner.nextDouble();
     }

     //This for determine the minimum, maximum of the marks
     sort(markValue); //sort the Array
     double minimumMark = markValue[0];
     double maximumMark = markValue[markNumber-1];

     //This for is for determine the mean of the marks
     double sumOfMarks = 0;
     for(double markValues : markValue)
     {
        sumOfMarks += markValues;
     }
     double markMean = sumOfMarks / markNumber;

     //Print the result
     System.out.printf("\nThe mean mark is:\t %1.2f \n", markMean);
     System.out.printf("The minimumMark is:\t %1.2f \n", minimumMark);
     System.out.printf("The maximumMark is:\t %1.2f \n\n", maximumMark);
     System.out.printf("Person | Score | difference from mean\n");

     //This for is for print the results
     for(int index = 0; index < markValue.length; index++)
     {
        double differenceMean = markValue[index] - markMean;
        System.out.printf("%6s |%6s |%6.2f%n", (index+1), markValue[index],
                          differenceMean);
     }//for
  }//main
  private static void sort(double[] arrayToSort)
  {
    //The unsorted length is reduced by one each time
    int unsortedLength = arrayToSort.length;
    boolean valueWasMoved;
    do {
       valueWasMoved = false;
       for(int index = 0; index < unsortedLength - 1;
           index++)
           if(arrayToSort[index] > arrayToSort[index + 1])
           {
              double valueToMove = arrayToSort[index];
              arrayToSort[index] = arrayToSort[index + 1];
              arrayToSort[index + 1] = valueToMove;
              valueWasMoved = true;
           }
    } while (valueWasMoved);
  }
}//class
