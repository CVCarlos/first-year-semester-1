/**
   This programs scan creates an interactive puzzle for the user to solve

   @author Carlos Cerda Veloz
*/

import java.io.File;
import java.util.Scanner;

public class RandomOrderPuzzle
{
  private String[] originalOrder;
  private String[] randomOrder;
  private int noOfLines;

  public static void main(String[] args) throws Exception
  {
    Scanner fileScanner = new Scanner(new File(args[0]));
    RandomOrderPuzzle puzzle = new RandomOrderPuzzle(fileScanner);

    Scanner inputScanner = new Scanner(System.in);
    System.out.println(puzzle);
    int moveCount = 0;
    while (! puzzle.isSorted())
    {
      System.out.print("Enter a line number to swap with the last one: ");
      puzzle.swapLine(inputScanner.nextInt());
      System.out.println(puzzle);
      moveCount++;
    }//while
    System.out.println("Game over in " + moveCount + " moves. ");
  }//main

  private void randomizeStringArrayOrder(String[] anArray)
  {
    for (int itemsRemaining = anArray.length;
         itemsRemaining > 0; itemsRemaining--)
    {
      int anIndex = (int) (Math.random() * itemsRemaining);
      String itemAtAnIndex = anArray[anIndex];
      anArray[anIndex] = anArray[anArray.length - 1];
      anArray[anArray.length - 1] = itemAtAnIndex;
    }//for
  }//randomizeStringArrayOrder

  private static final int INITIAL_ARRAY_SIZE = 2;
  private static final int ARRAY_RESIZE_FACTOR = 2;

  private RandomOrderPuzzle(Scanner scanner)
  {
    originalOrder = new String[INITIAL_ARRAY_SIZE];
    noOfLines = 0;
    while (scanner.hasNextLine())
    {
       originalOrder[noOfLines] = scanner.nextLine();
       if (noOfLines+1 == originalOrder.length)
       {
          String[] biggerArray 
            = new String[originalOrder.length * ARRAY_RESIZE_FACTOR];
          for (int index = 0; index < originalOrder.length; index++)
             biggerArray[index] = originalOrder[index]; 
          originalOrder = biggerArray;
       }
       noOfLines++;
    }
    randomOrder = new String[noOfLines];
    for(int index = 0; index < noOfLines; index++)
       randomOrder[index] = originalOrder[index];

    randomizeStringArrayOrder(randomOrder);
  }

  private boolean isSorted()
  {
     int check = 0;
     for(int index = 0; index < randomOrder.length; index++)
     {
        if(originalOrder[index] == randomOrder[index])
          check++;
     }
     return (check == randomOrder.length) ? true:false;
  }

  private void swapLine(int value)
  {
    String lastValue = randomOrder[noOfLines-1]; 
    randomOrder[noOfLines - 1] = randomOrder[value];
    randomOrder[value] = lastValue;
  }

  public String toString()
  {
    String result = "";
    for(int index = 0; index < randomOrder.length; index++)
    {
       result += String.format("%2d \t %s%n",index,randomOrder[index]);
    }    
    return result; 
  }

}//class
