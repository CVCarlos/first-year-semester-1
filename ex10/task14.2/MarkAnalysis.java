/**
  This program takes a list of sudents courseworks marks and produces a report

  @author Carlos Cerda Veloz
*/

import java.util.Scanner;

public class MarkAnalysis
{
  public static void main(String[] args)
  {
     Scanner inputScanner = new Scanner(System.in);

     //An scanner to get the marks
     System.out.print("Input the number of marks: ");
     int markNumber = inputScanner.nextInt();
     double[] markValue = new double[markNumber];

     //This for is for read the mark of each student
     for(int index = 0; index < markValue.length; index++)
     {
        System.out.printf("Enter mark # %2d: ", (index+1));
        markValue[index] = inputScanner.nextDouble();
     }

     //This for is for determine the minimum, maximum and mean of the marks
     double minimumMark = markValue[0];
     double maximumMark = markValue[0];
     double sumOfMarks = 0;
     for(int index = 0; index < markValue.length; index++)
     {
        if(markValue[index] < minimumMark)
           minimumMark = markValue[index];
        else if(markValue[index] > maximumMark)
           maximumMark = markValue[index];

        sumOfMarks += markValue[index];
     }
     double markMean = sumOfMarks / markNumber;

     //Print the result
     System.out.printf("\nThe mean mark is:\t %1.2f \n", markMean);
     System.out.printf("The minimumMark is:\t %1.2f \n", minimumMark);
     System.out.printf("The maximumMark is:\t %1.2f \n\n", maximumMark);
     System.out.printf("Person | Score | difference from mean\n");

     //This for is for print the results
     for(int index = 0; index < markValue.length; index++)
     {
        double differenceMean = markValue[index] - markMean;
        System.out.printf("%6s |%6s |%6.2f%n", (index+1), markValue[index],
                          differenceMean);
     }//for
  }//main

}//class
