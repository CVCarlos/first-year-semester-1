/**
  This class is used to save the information about a student

  @author Carlos Cerda Veloz
*/

public class Student
{
   private String name;
   private double mark;

   //Constructor method
   public Student(String nameStudent, double markStudent)
   {
      name = nameStudent;
      mark = markStudent;
   }//constructor method

   /**
     This method compare the mark of a student with another student
     If the mark is equal, then it will compate the nameStudent

     @param otherStudent Another student object
     @return The value will be 0 if equal, +ve if greater or -ve if less
   */
   public double compareTo(Student otherStudent)
   {
      if(mark == otherStudent.mark)
      return name.compareTo(otherStudent.name);
      else
      return mark - otherStudent.mark;
   }

   /**
     This method to print the information of the object with an specific
     format

     @return A format StudentName    got    mark 
   */
   public String toString()
   {
      return String.format("%-10s got %-6.2f",name, mark);
   }

   /**
      Access method for the name of the Student

      @return The name of the student
   */
   public String getName()
   {
      return name;
   }

   /**
      Access method for the mark of the Student

      @return The mark of the student
   */
   public double getMark()
   {
      return mark;
   }

}//class
