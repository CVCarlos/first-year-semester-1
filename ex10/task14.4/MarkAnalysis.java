/**
  This program takes a list of sudents courseworks marks and produces a report

  @author Carlos Cerda Veloz
*/

import java.util.Scanner;

public class MarkAnalysis
{
  public static void main(String[] args)
  {
     Scanner inputScanner = new Scanner(System.in);

     //An scanner to get the marks
     System.out.print("Enter the number of student: ");
     int studentNumber = inputScanner.nextInt();
     Student[] studentArray = new Student[studentNumber];

     //This for is for read the mark of each student
     for(int index = 0; index < studentNumber; index++)
     {
        inputScanner.nextLine();
        System.out.printf("Enter the name of student %2d: ", (index+1));
        String nameStudent = inputScanner.nextLine();
        System.out.printf("Enter mark for '%12s':",nameStudent);
        double markStudent = inputScanner.nextDouble();
        studentArray[index] = new Student(nameStudent, markStudent);
     }

     //This for determine the minimum, maximum of the marks
     sort(studentArray); //sort the Array
     double minimumMark = studentArray[0].getMark();
     double maximumMark = studentArray[studentNumber-1].getMark();

     //This for is for determine the mean of the marks
     double sumOfMarks = 0;
     for(Student markValues : studentArray)
     {
        sumOfMarks += markValues.getMark();
     }
     double markMean = sumOfMarks / studentArray.length;

     //Print the result
     System.out.printf("\nThe mean mark is:\t %1.2f \n", markMean);
     System.out.printf("The minimumMark is:\t %1.2f \n", minimumMark);
     System.out.printf("The maximumMark is:\t %1.2f \n\n", maximumMark);
     System.out.printf("Person and Score      | difference from mean\n");

     //This for is for print the results
     for(Student result : studentArray)
     {
        double differenceMean = result.getMark() - markMean;
        System.out.printf("%s | %6.2f%n",result,differenceMean);
     }//for
  }//main
  private static void sort(Student[] arrayToSort)
  {
    //The unsorted length is reduced by one each time
    int unsortedLength = arrayToSort.length;
    boolean valueWasMoved;
    do {
       valueWasMoved = false;
       for(int index = 0; index < unsortedLength - 1;
           index++)
           if(arrayToSort[index].compareTo(arrayToSort[index + 1]) > 0)
           {
              Student valueToMove = arrayToSort[index];
              arrayToSort[index] = arrayToSort[index + 1];
              arrayToSort[index + 1] = valueToMove;
              valueWasMoved = true;
           }
    } while (valueWasMoved);
  }
}//class
