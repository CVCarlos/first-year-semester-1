/**
   This program creates an stop clock which is displayed in a GUI

   *The stop clock include two buttons, one to start and stop the
    clock. An the other to split the time

   *The clock will display the start time, stop time, split time and elapsed
    time

   @author Carlos Cerda Veloz

*/

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class StopClock extends JFrame implements ActionListener
{
   //True for the clock running, false for the clock stopped
   private boolean isRunning = false;

   //Time the clock was started in milliseconds since 1970/1/1
   private long startTime = 0;

   //Time the clock was stopped in milliseconds since 1970/1/1
   private long stopTime = 0;

   //Time the clock was split in milliseconds since 1970/1/1
   private long splitTime = 0;

   //A label for the start time
   private final JTextField startTimeJTextField = new JTextField(50);

   //A label for the stop time
   private final JTextField stopTimeJTextField = new JTextField(50);

   //A label for elapsed time
   private final JTextField elapsedTimeJTextField = new JTextField(50);

   //A label for split time
   private final JTextField splitTimeJTextField = new JTextField(50);

   //Instance variable to start and stop the clock
   private JButton startStopJButton;

   private JButton splitTimeJButton;

   /**
      Constructor method
   */
   public StopClock()
   {
      setTitle("Stop Clock");

      Container contents = getContentPane();

      //The grid layour is 1 column
      contents.setLayout(new GridLayout(0,1));

      contents.add(new JLabel("Started at:"));
      contents.add(startTimeJTextField);
      startTimeJTextField.setEnabled(false);

      contents.add(new JLabel("Stopped at:"));
      contents.add(stopTimeJTextField);
      stopTimeJTextField.setEnabled(false);

      contents.add(new JLabel("Elapsed time (seconds):"));
      contents.add(elapsedTimeJTextField);
      elapsedTimeJTextField.setEnabled(false);

      contents.add(new JLabel("Split time (seconds):"));
      contents.add(splitTimeJTextField);
      splitTimeJTextField.setEnabled(false);

      splitTimeJButton = new JButton("Split");
      splitTimeJButton.addActionListener(this);
      contents.add(splitTimeJButton);
      splitTimeJButton.setEnabled(false);

      startStopJButton = new JButton("Start");
      startStopJButton.addActionListener(this);
      contents.add(startStopJButton);

      setDefaultCloseOperation(EXIT_ON_CLOSE);
      pack();
   }//StopClock

   /**
      	This method has information about the action that need to be
        performed once the button is pressed

        @param event This variable represents the action of push the
                     button
   */
   public void actionPerformed(ActionEvent event)
   {
      if(event.getSource() == startStopJButton)
      {
         if(!isRunning)
         {
            //Start the clock
            splitTimeJButton.setEnabled(true);
            startStopJButton.setText("Stop");
            startTime = System.currentTimeMillis();
            startTimeJTextField.setText("" + startTime);
            stopTimeJTextField.setText("Running...");
            elapsedTimeJTextField.setText("Running...");
            isRunning = true;
         }//if
         else //isRunning
         {
            //Stop the clock, show times
            splitTimeJButton.setEnabled(false);
            startStopJButton.setText("Start");
            stopTime = System.currentTimeMillis();
            stopTimeJTextField.setText("" + stopTime);
            long elapsedMilliSeconds = stopTime - startTime;
            elapsedTimeJTextField.setText("" + elapsedMilliSeconds / 1000.0);
            isRunning = false;
         }//else
      }
      else //getSource == splitTimeJButton
      {
         if(isRunning)
         {
            splitTime = System.currentTimeMillis();
            long splitMilliSeconds = splitTime - startTime;
            splitTimeJTextField.setText("" + splitMilliSeconds / 1000.0);
         }
      }

      pack();
   }//actionPerfomed

   /**
      Create an Stop Clock and display this in the screen
   */
   public static void main(String[] args)
   {
      StopClock theStopClock = new StopClock();
      theStopClock.setVisible(true);
   }//main

}//class
