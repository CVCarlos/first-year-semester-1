/**
   This program calculate the great common divisor of three numbers

   @author Carlos Cerda Veloz
*/

public class MyMath
{
  /**
     This method calculates the GCD between three numbers

     @param number1 The first number
     @param number2 The seconds number
     @param number2 The third number
     @return The method returns the GCD
  */
  public static int greatCommonDivisor(int number1, int number2, int number3)
  {
    int compareGreater = number1;
    int compareSmaller = number2;

    //Here, if the result of compare the two first ages is equal the GCD get
    //the value of whatever of the two ages.

    int temporalgreatCommonDivisor = number1;

    //Here the program start calculating the GCD of the first ages

    while(compareGreater != compareSmaller)
    {
      if (compareGreater > compareSmaller)
      {
        compareGreater = compareGreater - compareSmaller;
        temporalgreatCommonDivisor = compareGreater;
      }
      else
      {
        compareSmaller = compareSmaller - compareGreater;
        temporalgreatCommonDivisor = compareSmaller;
      }
    }

    //Here, if the result of compare the third age and the temporal GCD is equal
    //the GCD get the value of whatever of the two values.

    int greatCommonDivisor = number3;

    compareGreater = temporalgreatCommonDivisor;
    compareSmaller = number3;

    //Here the program start calculating the final GCD

    while(compareGreater != compareSmaller)
    {
      if (compareGreater > compareSmaller)
      {
        compareGreater = compareGreater - compareSmaller;
        greatCommonDivisor = compareGreater;
      }
      else
      {
        compareSmaller = compareSmaller - compareGreater;
        greatCommonDivisor = compareSmaller;
      }
    }
    return greatCommonDivisor;
  }//greatCommonDivisor

}//class
