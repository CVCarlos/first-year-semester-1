/**
   This program calculate the great common divisor of three numbers
   which is displayed in a GUI

   *The GCD include one button that activates the events for calculate
   the GCD

   *The clock will display the start time, stop time, split time and elapsed
    time

   @author Carlos Cerda Veloz

*/

import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class GCD extends JFrame implements ActionListener
{
   //Texts field for each number
   private final JTextField number1JTextField = new JTextField(20);
   private final JTextField number2JTextField = new JTextField(20);
   private final JTextField number3JTextField = new JTextField(20);

   //Text field for the result
   private final JTextField resultJTextField = new JTextField(20);

   /**
      This is a constructor method for the GCD
   */
   public GCD()
   {
      setTitle("GCD");
      Container contents= getContentPane();
      contents.setLayout(new GridLayout(0, 1));

      contents.add(new JLabel("Number1"));
      contents.add(number1JTextField);

      contents.add(new JLabel("Number 2"));
      contents.add(number2JTextField);

      contents.add(new JLabel("Number 3"));
      contents.add(number3JTextField);

      JButton computeJButton = new JButton("Compute");
      contents.add(computeJButton);
      computeJButton.addActionListener(this);

      contents.add(new JLabel("GCD of Number 1,Number 2 and Number 3"));
      contents.add(resultJTextField);

      setDefaultCloseOperation(EXIT_ON_CLOSE);
      pack();
   }//GCD

   /**
      This method containes the actions to be performed one the button is
      press
   */
   public void actionPerformed(ActionEvent event)
   {
      int number1 = Integer.parseInt(number1JTextField.getText());
      int number2 = Integer.parseInt(number2JTextField.getText());
      int number3 = Integer.parseInt(number3JTextField.getText());

      int theGCD = MyMath.greatCommonDivisor(number1, number2, number3);
      resultJTextField.setText("" + theGCD);
   }//actionPerformed

   /**
      Create a GCD and display this in the screen
   */
   public static void main(String[] args)
   {
      GCD theGCD = new GCD();
      theGCD.setVisible(true);
   }//main
}//class
