/**
   This programs allows an user to input three numbers which represents
   three weights.
   The program determines all the possible outputs that can be done using that
   three weigths.
   The input and the result is made using a GUI

   @author Carlos Cerda Veloz
*/

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

public class ThreeWeights extends JFrame implements ActionListener
{
   //JTextField for the three weigths
   private final JTextField weight1JTextField = new JTextField(20);
   private final JTextField weight2JTextField = new JTextField(20);
   private final JTextField weight3JTextField = new JTextField(20);

   //JTextField for the outputs
   private final JTextField resultJTextField = new JTextField(20);

   //JTextArea for the outputs
   private final JTextArea displayJTextArea = new JTextArea(15,20);

   /**
      Constructor method
   */
   public ThreeWeights()
   {
     setTitle("Three Weights");
     Container contents = getContentPane();
     contents.setLayout(new GridLayout(0, 1));

     JPanel weightFieldJPanel = new JPanel();
     contents.add(weightFieldJPanel);
     weightFieldJPanel.setLayout(new GridLayout(0,3));

     JPanel buttonAndResultFieldJPanel = new JPanel();
     contents.add(buttonAndResultFieldJPanel);
     buttonAndResultFieldJPanel.setLayout(new GridLayout(0,2));

     weightFieldJPanel.add(new JLabel("Weight 1"));
     weightFieldJPanel.add(new JLabel("Weight 2"));
     weightFieldJPanel.add(new JLabel("Weight 3"));
     weightFieldJPanel.add(weight1JTextField);
     weightFieldJPanel.add(weight2JTextField);
     weightFieldJPanel.add(weight3JTextField);

     JButton computeJButton = new JButton("Compute");
     buttonAndResultFieldJPanel.add(computeJButton);
     computeJButton.addActionListener(this);

     JPanel resultJPanel = new JPanel();
     resultJPanel.setLayout(new GridLayout(0,1));
     resultJPanel.add(new JScrollPane(displayJTextArea));
     buttonAndResultFieldJPanel.add(resultJPanel);

     setDefaultCloseOperation(EXIT_ON_CLOSE);
     pack();
   }//Weigts

   /**
      This method performs the actions once the button is
      pressed

      @param event This variable represents the action
                  of press the button
   */
   public void actionPerformed(ActionEvent event)
   {
     displayJTextArea.setText("");

     int weight1 = Integer.parseInt(weight1JTextField.getText());
     int weight2 = Integer.parseInt(weight2JTextField.getText());
     int weight3 = Integer.parseInt(weight3JTextField.getText());

     displayJTextArea.append("----------------------------------\n");
     displayJTextArea.append("|           Results         |\n");
     for(int index1 = -1; index1 <= 1; index1++)
     {
       int weight1Index = weight1 * index1;
       for(int index2 = -1; index2 <= 1; index2++)
       {
         int weight2Index = weight2 * index2;
         for(int index3 = -1; index3 <= 1; index3++)
         {
           int weight3Index = weight3 * index3;
           displayJTextArea.append(weight1Index + "  " + weight2Index + "  "
                                   + weight3Index + " = ");
           displayJTextArea.append("" + (weight1Index + weight2Index
                                  + weight3Index) + "\n");
         }
       }
     }
     displayJTextArea.append("----------------------------------\n");
   }//actionPerformed

   /**
      Create a Three Weights and display this in the screen
   */
   public static void main(String[] args)
   {
     new ThreeWeights().setVisible(true);
   }//main
}
