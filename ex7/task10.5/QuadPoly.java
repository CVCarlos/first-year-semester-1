public class QuadPoly
{
   private double first, second, third;

   public QuadPoly (double firstCoefficient, double secondCoefficient, 
                    double thirdCoefficient)
   {
      first = firstCoefficient;
      second = secondCoefficient;
      third = thirdCoefficient;
   }//Constructor-method

   //This method add both polynomials
   public QuadPoly addPolynomial(QuadPoly otherPolynomial)
   {  
      double sumFirst  = first + otherPolynomial.first;
      double sumSecond = second + otherPolynomial.second;
      double sumThird  = third + otherPolynomial.third;

      return new QuadPoly (sumFirst, sumSecond, sumThird);

   }//addPolynomial

   public String toString()
   {  
      return first + "x^2 + " + second + "x + " + third;
   }//toString

   


}
