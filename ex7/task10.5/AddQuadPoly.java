//This program got two polynomials and used an object clss to create instances
//of the polynomials coefficients. 
//Then the program sum the two polynomials and print the results
//Made by: Carlos Andres Cerda Veloz, group W
public class AddQuadPoly
{
   public static void main(String[] args)
   {
      QuadPoly polynomial1 = new QuadPoly(Double.parseDouble(args[0]), 
                                          Double.parseDouble(args[1]),
                                          Double.parseDouble(args[2]));

      QuadPoly polynomial2 = new QuadPoly(Double.parseDouble(args[3]), 
                                          Double.parseDouble(args[4]),
                                          Double.parseDouble(args[5]));

      QuadPoly polynomial3 = polynomial1.addPolynomial(polynomial2);

      printResults(polynomial1, polynomial2, polynomial3);

   }//main
   
   //This method print the results
   private static void printResults(QuadPoly polynomial1, QuadPoly polynomial2, 
                                    QuadPoly polynomial3)
   {
      System.out.println("Polynomial:    " + polynomial1.toString());

      System.out.println("added to:      " + polynomial2.toString());

      System.out.println("results in:    " + polynomial3.toString());
   }//printResults


}//class
