/*
   This class is used to create an object with the points
   x and y. The class is used for other classes two create 
   the shapes.
   Program made by Carlos Cerda Veloz, group W
*/

public class Point
{
   private double x, y;

   //Constructor method
   public Point (double pointX, double pointY)
   {
   x = pointX;
   y = pointY;
   }
   
   public double getX() 
   {
   return x;
   }

   public double getY() 
   {
   return y;
   }

   //This method shift the shape 
   public Point shift (double xShift, double yShift)
   {
   return new Point(x + xShift, y + yShift); 
   }

   //This method determine the distance between two
   //points
   public double distance(Point secondPoint)
   {
   return Math.sqrt(Math.pow((secondPoint.x - x),2) + 
                    Math.pow((secondPoint.y - y),2)); 
   } 

   //This method print the results 
   public String toString()
   {
   return "((" + x + "," + y + ")";
   }
   
}
