/*
   This class is used to create a triangle
   A Triangle is specified by giving the X and Y coordinates of each of
   its three corner points.
   Program made by Carlos Cerda Veloz, group W
*/

public class Triangle
{
   private final Point point1, point2, point3;
   
   //Constructor method
   public Triangle (Point pointA, Point pointB, Point pointC)
   {
   point1 = pointA;
   point2 = pointB;
   point3 = pointC;
   }

   //This method shift the shape using the Point class
   public Triangle shift (double xShift, double yShift)
   {
   return new Triangle (point1.shift(xShift,yShift),
                        point2.shift(xShift,yShift),
                        point3.shift(xShift,yShift));                       
   }

   //The methods side# calculate the distance between two points
   //using the Point class 
   public double side1()
   {   
   return point1.distance(point2);
   }

   public double side2()
   {
   return point1.distance(point3);
   }

   public double side3()
   {
   return point2.distance(point3);
   }  

    //This method calculate the area of the shape
   public double area()
   {

   double semiPerimeter = (side1() + side2() + side3())/2;
   double areaResult = Math.sqrt(semiPerimeter * (semiPerimeter - side1())
                       * (semiPerimeter - side2())
                       * (semiPerimeter - side3()));
   return areaResult;
   }

   //This method calculate the permiter of the shape
   public double perimeter()
   {

   return side1() + side2() + side3();
   }

   //This method print the results using the Point class
   public String toString()
   {
   return point1 + "," + point2 + "," + point3 + ")";
   }

}
