/*
   This class is used to create a rectangle 
   A rectangle (always axis-aligned) is specified by giving the X and Y
   coordinates of two of its diagonally opposite corner points.
   Program made by Carlos Cerda Veloz, group W
*/

public class Rectangle
{
   private final Point point1, point2;

   //Constructor method   
   public Rectangle (Point diag1End1, Point diag1End2)
   {
   point1 = diag1End1;
   point2 = diag1End2;  
   }
   
   //This method shift the shape using the Point class
   public Rectangle shift (double xShift, double yShift)
   {
   return new Rectangle (point1.shift(xShift,yShift),
                         point2.shift(xShift,yShift));
   }

   //The methods side# calculate the distance between two points
   //using the Point class 
   public double side1()
   {
   return point1.distance(new Point (point1.getX(),point2.getY()));
   }

   public double side2()
   {
   return point1.distance(new Point (point2.getX(), point1.getY()));
   }

   //This method calculate the area of the shape
   public double area()
   {
   return side1() * side2();
   }

   //This method calculate the permiter of the shape
   public double perimeter()
   {
   return 2 * (side1() + side2());
   }
   
   //This method print the results using the Point class
   public String toString()
   {
   return point1 + ",(" + point2.getX() + "," + point1.getY() + ")," + point2 +
          "," + "(" + point1.getX() + "," + point2.getY() + "))";
   }
}
