/*
   This class is used to create a circle   
   A circle is specified by giving the X and then Y coordinate of its
   centre, followed by its radius.
   Program made by Carlos Cerda Veloz, group W
*/

public class Circle
{
   private final Point point1; 
   private final double r;

   //Constructor method
   public Circle (Point centre, double radius)
   {
   point1 = centre;
   r = radius;
   }

   //This method shift the shape using the Point class
   public Circle shift (double xShift, double yShift)
   {
   return new Circle (point1.shift(xShift,yShift),r);
   }

   //This method calculate the area of the shape
   public double area()
   {
   return Math.PI * r;
   }

   //This method calculate the permiter of the shape
   public double perimeter()
   {
   return 2 * Math.PI * r;
   }

   //This method print the results using the Point class
   public String toString()
   {
   return point1 + "," + r + ")";
   }
}
