//This programs print a 19 times 19 labelled table indiciating
//which of all the pairs made up of inegers between 2 and 20
//have common factors bigger than one.
// -#- represents a factor bigger than one and -|- the opposite 

public class CommonFactorsTable
{
   public static void main(String[] args)
   {

   printline();
   printColumn();
   printline();
   printRow(); 
    }
   private static void printline()
   {
     System.out.print("|-----|");
     for (int indexColumn = 1; indexColumn < 20 ; indexColumn++)
     System.out.print("----");     
     System.out.println("-|");
   }  
   private static void printColumn()
   {
     System.out.print("|     |");
     for (int indexColumnNumbers = 2; indexColumnNumbers <= 20;
          indexColumnNumbers++)
     {
     printNumber(indexColumnNumbers);
     }
     System.out.println(" |");
   }	    
   private static void printRow()
   {
     for (int indexRowNumbers = 2; indexRowNumbers <= 20; indexRowNumbers++)
     {    
       System.out.print("|");
       printNumber(indexRowNumbers);
       System.out.print(" |-");
       calculateGCD(indexRowNumbers);  
     }        
   }
   private static void printNumber(int number)
   {
   if (number < 10)
   System.out.print("   " + number);
   else
   System.out.print("  " + number);
   }
   private static void calculateGCD(int rowNumber)
   {
      for (int indexColumnNumber = 2; indexColumnNumber <= 20; indexColumnNumber++)
      {
        int compareGreater = indexColumnNumber;
        int compareSmaller = rowNumber;

        while(compareGreater != compareSmaller)
        {
           if (compareGreater > compareSmaller)
           compareGreater = compareGreater - compareSmaller;                     
           else
           compareSmaller = compareSmaller - compareGreater;                
        }
        rowResults(compareGreater, indexColumnNumber);
      }
      System.out.println("|");
   } 
   private static void rowResults(int GCD,
                                  int columnNumber)
   { 
       if (columnNumber < 10)
       {
       if (GCD > 1)
       System.out.print("--#-");
       else 
       System.out.print("--|-");       
       }
       else
       { 
       if (GCD > 1)
       System.out.print("--#-");
       else 
       System.out.print("--|-");  
       } 
   }    
}
       
    
