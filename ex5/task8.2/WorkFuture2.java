public class WorkFuture2
{
   public static void main(String[] args)
   {
     //Input: Present Year, birth years of frist and second person
     int presentYear = Integer.parseInt(args[0]);
     int birthYearFirstPerson = Integer.parseInt(args[1]);
     int birthYearSecondPerson = Integer.parseInt(args[2]);

     //Calculate years before retirement of the first person

     int yearsLeft = 68 - (presentYear - birthYearFirstPerson);

     System.out.println("The first person has " + yearsLeft + " years left to work");

     //Printing out the work future time of the first person

     int yearIndex = presentYear;

     for (int yearsLeftIndex = yearsLeft - 1; yearsLeftIndex >= 1; yearsLeftIndex--)
     {
       yearIndex++;
       System.out.println("In " + yearIndex + " the first person will have " + yearsLeftIndex + " years left to work");       
     }

     System.out.println("First person will retire in " + (presentYear + yearsLeft) );

     //Calculate years before retirement of the first person
     
     yearsLeft = 68 - (presentYear - birthYearSecondPerson);

     System.out.println("The second person has " + yearsLeft + " years left to work");
     
     //Printing out the work future time of the first person

     yearIndex = presentYear;

     for (int yearsLeftIndex = yearsLeft - 1; yearsLeftIndex >= 1; yearsLeftIndex--)
     {
       yearIndex++;
       System.out.println("In " + yearIndex + " the first person will have " + yearsLeftIndex + " years left to work");       
     }
     System.out.println("Second person will retire in " + (presentYear + yearsLeft) );     
   }
}
