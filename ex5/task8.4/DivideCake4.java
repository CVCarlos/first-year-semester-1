//This program takes three ages and determines the Great Common Divisor (GCD) 
//of the three then this return in how many slices a cake need to be divided so
//each of the tree ages get an proportional number of slices
public class DivideCake4
{
   private static int greatCommonDivisor(int compareGreater, 
                                        int compareSmaller)
   {
                                        
     //If the result of compare the two values is equal the GCD 
     //get the value of whatever of the two ages.

     //The compare variables are used to save the results
     //when calculating the GCD    

          
     //Here the program start calculating the GCD of the first ages

     while(compareGreater != compareSmaller)
     {
       if (compareGreater > compareSmaller)
       compareGreater = compareGreater - compareSmaller;        
       else
       compareSmaller = compareSmaller - compareGreater;        
     }

     return compareGreater;
   }

   public static void main(String[] args)
   {

     //Input of thee ages 

     int firstAge = Integer.parseInt(args[0]);
     int secondAge = Integer.parseInt(args[1]);
     int thirdAge = Integer.parseInt(args[2]);
     int fourthAge = Integer.parseInt(args[3]);

     //Calling the method to calculate the GCD
     
     int GCD = greatCommonDivisor(firstAge, secondAge);
     GCD = greatCommonDivisor(GCD, thirdAge);
     GCD = greatCommonDivisor(GCD, fourthAge);
     
          
   
     int portionFirstAge = firstAge / GCD;
     int portionSecondAge = secondAge / GCD;
     int portionThirdAge = thirdAge / GCD;
     int portionFourthAge = fourthAge / GCD;

     int cakeDivisions = portionFirstAge + portionSecondAge + portionThirdAge
                         + portionFourthAge ;

     //Print out of the results

     System.out.println("The GCD of " + firstAge + ", " + secondAge + " , " 
                        + thirdAge + " and " + fourthAge + " is " 
                        + GCD);
     System.out.println("The cake should be divided into " + cakeDivisions);
     System.out.println("The " + firstAge + " years old gets " + portionFirstAge
                        + " , the " + secondAge + " years old gets " 
                        + portionSecondAge + " ,the " + thirdAge 
                        + " years old gets " + portionThirdAge + " and the "
                        + fourthAge + " years old gets " + portionFourthAge);    
     
   }
}
     
     
