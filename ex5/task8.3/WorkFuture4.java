public class WorkFuture4
{
   public static void main(String[] args)
   {
     //Input: Present Year, birth years of frist and second person
     int presentYear = Integer.parseInt(args[0]);
     int birthYearFirstPerson = Integer.parseInt(args[1]);
     int birthYearSecondPerson = Integer.parseInt(args[2]);
     int birthYearThirdPerson = Integer.parseInt(args[3]);
     int birthYearFourthPerson = Integer.parseInt(args[4]);

     //Calculate years before retirement of the first person
     
     printWorkFuture(presentYear, 1, birthYearFirstPerson);
     printWorkFuture(presentYear, 2, birthYearSecondPerson);
     printWorkFuture(presentYear, 3, birthYearThirdPerson);
     printWorkFuture(presentYear, 4, birthYearFourthPerson);
   }

     private static void printWorkFuture(int presentYear, int personNumber,
                                         int birthYearPerson)
     {

     int yearsLeft = 68 - (presentYear - birthYearPerson);

     System.out.println("The person number " + personNumber + " has "
                         + yearsLeft + " years left to work");

     //Printing out the work future time of the first person

     int yearIndex = presentYear;

     for (int yearsLeftIndex = yearsLeft -1; yearsLeftIndex >= 1; 
          yearsLeftIndex--)
     {
       yearIndex++;
       System.out.println("In " + yearIndex + " the first person will have "
                           + yearsLeftIndex + " years left to work");       
     }

     System.out.println("First person will retire in "
                        + (presentYear + yearsLeft) );
   }
}
