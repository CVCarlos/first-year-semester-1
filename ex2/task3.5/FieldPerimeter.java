public class FieldPerimeter
{
   public static void main(String[] args)
   {
     int length = Integer.parseInt(args[0]);
     int width  = Integer.parseInt(args[1]);
     
     int perimeter = length + length + width + width;

     System.out.println("The length of fence needed to enclose the field is " + perimeter);
    }
}
