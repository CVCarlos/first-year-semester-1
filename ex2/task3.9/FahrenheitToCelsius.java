public class FahrenheitToCelsius
{
   public static void main(String[] args)
   {
     double FahrenheitDegrees = Double.parseDouble(args[0]);
     
     double CelsiusDegrees = (FahrenheitDegrees - 32) / 1.8;
     
     System.out.println("The temperature " + FahrenheitDegrees + " in Fahrenheit is "
                        + CelsiusDegrees + " in Celsius");
   }
}
