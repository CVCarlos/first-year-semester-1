public class RoundPennies
{
   public static void main(String[] args)
   {
     int pennies = Integer.parseInt(args[0]);
      
     int pounds  = (pennies + 50) / 100;

     System.out.println("The about pounds of " + pennies + " is " + pounds);
   }
}
