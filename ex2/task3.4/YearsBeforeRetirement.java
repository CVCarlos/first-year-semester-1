public class YearsBeforeRetirement
{
   public static void main(String[] args)
   {
     int MyAgeNow;
     int AgeOfRetirement;
     int YearsLeft;
     MyAgeNow = Integer.parseInt(args[0]);
     AgeOfRetirement = 68;
     YearsLeft = 68 - 20;
     System.out.println("My age now is " + MyAgeNow);
     System.out.println("I will retire at the age of " + AgeOfRetirement);
     System.out.println("Years left working is " + YearsLeft);
   }
}
