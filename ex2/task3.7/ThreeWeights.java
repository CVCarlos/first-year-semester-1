public class ThreeWeights
{
   public static void main(String[] args)
   {
     int weight1 = Integer.parseInt(args[0]);
     int weight2 = Integer.parseInt(args[1]);
     int weight3 = Integer.parseInt(args[2]);
     
     System.out.println(-weight1 - weight2 - weight3);
     System.out.println(-weight1 - weight2          );
     System.out.println(-weight1 - weight2 + weight3);
     System.out.println(-weight1           - weight3);
     System.out.println(-weight1 	            );
     System.out.println(-weight1 	   + weight3);
     System.out.println(-weight1 + weight2 - weight3);
     System.out.println(-weight1 + weight2          );
     System.out.println(-weight1 + weight2 + weight3);
     System.out.println(         - weight2 - weight3);
     System.out.println(	 - weight2          );
     System.out.println(	 - weight2 + weight3);
     System.out.println(		   - weight3);
     System.out.println(0			    );
     System.out.println(		   + weight3);
     System.out.println(	 + weight2 - weight3);
     System.out.println(	 + weight2          );
     System.out.println(	 + weight2 + weight3);
     System.out.println( weight1 - weight2 - weight3);
     System.out.println( weight1 - weight2          );
     System.out.println( weight1 - weight2 + weight3);
     System.out.println( weight1 	   - weight3);
     System.out.println( weight1		    );
     System.out.println( weight1           + weight3);
     System.out.println( weight1 + weight2 - weight3);
     System.out.println( weight1 + weight2          );
     System.out.println( weight1 + weight2 + weight3);
    }
}
