/** 
   This program prints a Hello Family
   message making use 
   of a GUI interface
   
   @author Carlos Cerda Veloz
*/

import java.util.Scanner;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelloFamily extends JFrame
{   
   private static
   Scanner inputScanner = new Scanner(System.in);

   /** 
      This method construct the Hello Family object
      
      @param rows The wanted number of rows
      @param columns The wanted numbers of columns 
   */
   public HelloFamily(int rows, int columns)
   {
     setTitle("Hello Family");
     Container contents = getContentPane();
     contents.setLayout(new GridLayout(rows,columns,50,20));
     contents.add(new JLabel("Hello Alberto"));
     contents.add(new JLabel("Hello Carlos"));
     contents.add(new JLabel("Hello Carlos Andres"));
     contents.add(new JLabel("Hello Emma"));
     contents.add(new JLabel("Hello Galo"));
     contents.add(new JLabel("Hello Lalita"));
     contents.add(new JLabel("Hello Paola"));
     contents.add(new JLabel("Hello Ramiro"));

     setDefaultCloseOperation(EXIT_ON_CLOSE);
     pack();
    }

   /** 
      This method takes two integer
      commans lines arguments and 
      prints the message 
   */
   public static void main(String[] args)
   {
      System.out.println("Input values for rows and columns");
      int rows = inputScanner.nextInt();
      int columns = inputScanner.nextInt();
      HelloFamily family = new HelloFamily(rows, columns);
      family.setVisible(true);
   }   
}
