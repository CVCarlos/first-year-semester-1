/** 
   This program prints a Hello Family
   message making use 
   of a GUI interface
   
   @author Carlos Cerda Veloz
*/

import java.awt.Container;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelloFamily extends JFrame
{
   /** 
      This method construct the 
      Hello Family object
   */
   public HelloFamily()
   {
     setTitle("Hello Family");
     Container contents = getContentPane();
     contents.setLayout(new FlowLayout());
     contents.add(new JLabel("Hello Alberto"));
     contents.add(new JLabel("Hello Carlos"));
     contents.add(new JLabel("Hello Carlos Andres"));
     contents.add(new JLabel("Hello Emma"));
     contents.add(new JLabel("Hello Galo"));
     contents.add(new JLabel("Hello Lalita"));
     contents.add(new JLabel("Hello Paola"));
     contents.add(new JLabel("Hello Ramiro"));

     setDefaultCloseOperation(EXIT_ON_CLOSE);
     pack();
    }

   /** 
      This method prints the message
   */
   public static void main(String[] args)
   {
      HelloFamily family = new HelloFamily();
      family.setVisible(true);
   }   
}
