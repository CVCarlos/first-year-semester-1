/** 
   This program prints a Hello World 
   message in Spanish and make use 
   of a GUI interface
   
   @author Carlos Cerda Veloz
*/

import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HelloWorld extends JFrame
{
   /** 
      This method construct the Hello World object
   */
   public HelloWorld()
   {
      setTitle("Hello World");
      Container contents = getContentPane();
      contents.add(new JLabel("Hola Mundo"));
      setDefaultCloseOperation(EXIT_ON_CLOSE);
      pack();
   }

   /** 
      This method prints the message
   */
   public static void main(String[] args)
   {
      HelloWorld spanish = new HelloWorld();
      spanish.setVisible(true);
   }   
}
