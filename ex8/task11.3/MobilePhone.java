public class MobilePhone
{
   private final String model;
   private int seconds;
   private final Account account;

   public MobilePhone (String modelPhone, int secondsPhone, Account accountPhone)
   {
      model = modelPhone;
      seconds = secondsPhone;
      account = accountPhone;
   }

   public String toString()
   {
      return "Phone(" + model + "," + seconds + "," + account + ")"; 
   }

  public String secondsCheck(int secondsCall)
  {
      String result = account.SecondsCall(secondsCall);
      seconds += account.getSeconds();
      return result;
  }

  public String topUpCheck(int topUp)
  {
      return account.topUpCheck(topUp);
  }

}//class

