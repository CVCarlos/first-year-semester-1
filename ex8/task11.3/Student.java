/*
   This class create an object of each students
   that is created.
   Program made by Carlos Cerda Veloz, group W
*/
public class Student
{ 
   //Name of the student
   private final String name;

   //Information about the phone and account
   private MobilePhone phone; 

   //Line separator
   private static String NLS = System.getProperty("line.separator");
   
   //Constructor method
   public Student (String nameStudent, MobilePhone phoneStudent)
   {
      name = nameStudent;
      phone = phoneStudent;
   }

   //Print the Student information
   public String toString()
   {
      return "Student("+ name+ "," + phone + ")";
   }

   //Determines if the call can be applied
   public String studentCall(int seconds)
   {
      if (phone == null)
      return "This next call has no effect, as has no phone!" + NLS 
             + toString();
      else
      return phone.secondsCheck(seconds) + toString();
   }

   //Determines if the top is valid
   public String topUpStudent(int topUp)
   {
      if (phone == null)
      return "This next top up has no effect, as has no phone!" + NLS 
             + toString();
      else
      return toString() + phone.topUpCheck(topUp);
   }   
}//class
