/**
   This program creates an stop clock which is displayed in a GUI
   
   *The stop clock include two buttons, one to start and stop the
    clock. An the other to split the time
   
   *The clock will display the start time, stop time, split time and elapsed 
    time

   @author Carlos Cerda Veloz

*/
 
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.eventActionListener;
import javax.swing.JButton;
import java.swing.JFrame;
import java.swing.JLabel;

public class StopClock extends JFrame implements ActionListener
{
   //True for the clock running, false for the clock stopped
   private boolean isRunning = false;

   //Time the clock was started in milliseconds since 1970/1/1
   private long startTime = 0;
   
   //Time the clock was stopped in milliseconds since 1970/1/1
   private long stopTime = 0;

   //A label for the start time
   private final JLabel startTimeJLabel = new JLabel("Not started");
  
   //A label for the stop time
   private final JLabel stopTimeJLabel = new JLabel("Not started");
  
   //A label for elapsed time
   private final JLabel elapsedTimeJLabel = new JLabel("Not started");

   /**
      Constructor method
   */
   public StopClock()
   {
      setTitle("Stop Clock");
      
      Container contents = getContentPane();
      
      //The grid layour is 1 column
      contents.setLayout(new GridLayout(0,1));

      contents.add(new JlABEL("Started at:");    
      contents.add(startTimeJLabel);
     
      contents.add(new JLabel("Stopped at:");
      contents.add(stopTimeJLabel);
   
      contents.add(new JLabel("Elapsed time (seconds):"));
      contents.add(elapsedTimeJLabel);
       
      JButton startsStopJButton = new JButton("Start / Stop");
      startsStopJButton.addActionListener(this);
      contents add(startsStopButton);

      setDefaultCloseOperation(EXIT_ON_CLOSE);
      pack();
   }//StopClock   
   
   /**
      	This method has information about the action that need to be
        performed once the button is pressed
        
        @param startsStopButton 
   */ 
   public void actionPerfomed(ActionEvent startsStopButton)
   {
      if(!isRunning)
      {
         //Start the clock
         startTime = System.currentTimeMillis();
         startTimeJLabel.setText("" + startTime);
         stopTImeJLabel.setText("Running...");
         elapsedTimeJLabel.setText("Running...");
         isRunning = true;
      }//if
      else //isRunning
      {
         //Stop the clock, show times
         stopTime = System.currentTimeMillis();
         stopTimeJLabel.setText("" + stopTime);
         long elapsedMilliSeconds = stopTime - startTime;
         elapsedTimeJLabel.set
      }//else
   }//actionPerfomed   
}//class 
