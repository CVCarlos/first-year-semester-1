/* This program simulates a sceneraio 
   
   First three students are created
   *   The students can buy a mobile phone which includes an account.

   *   This account start with a credit of 0, the credit is showed in seconds

   *   The students can top up their account using pounds, which are added to
       their phones accounts as seconds.

   *   Each time that they made a call, the duration of this in seconds is
       reduced from their accounts and added to the register of the phone.

   *   If the students try to do a call or top up without a cellphone a message 
       of error is printed.

   *   If the call duration is more than the seconds in the account, then the
       call is truncated.

   Program created by: Carlos Cerda Veloz
*/
public class StudentsCalling
{
   public static void main (String[] args)
   {

      System.out.println("Creating student Carlos");
      String nameStudent1 = "Carlos";
      Student student1 = new Student(nameStudent1,null);   
      StudentPrint(student1);
  
      System.out.println("Creating student Charles");
      String nameStudent2 = "Charles";
      Student student2 = new Student(nameStudent2,null);
      StudentPrint(student2);

      System.out.println("Creating student Diana");
      String nameStudent3= "Diana";
      Student student3 = new Student(nameStudent3,null);
      StudentPrint(student3);

      studentCall(student1);
      studentCall(student2);
      studentCall(student3); 

      studentTopUp(student1);
      studentTopUp(student2);
      studentTopUp(student3);

      System.out.println(student1);   
      String phone1= "Samsung S300";    
      System.out.println("is buying a new phone " +  phone1);  
      String account1= "Voodo"; 
      System.out.println("with account " + account1);
      Account accountStudent1 = new Account(0, account1);
      MobilePhone phoneStudent1 = new MobilePhone (phone1, 0 , accountStudent1);
      student1 = new Student(nameStudent1,phoneStudent1);
      StudentPrint(student1);

      System.out.println(student2);
      String phone2= "Aphone X";   
      System.out.println("is buying a new phone " + phone2);  
      String account2= "Raspberry"; 
      System.out.println("with account " + account2);
      Account accountStudent2 = new Account(0, account2);
      MobilePhone phoneStudent2 = new MobilePhone (phone2, 0 , accountStudent2);
      student2 = new Student(nameStudent2,phoneStudent2);
      StudentPrint(student2);

      System.out.println(student3);
      String phone3 = "Surfer 3000";  
      System.out.println("is buying a new phone " + phone3);  
      String account3 = "Three"; 
      System.out.println("with account " + account3);
      Account accountStudent3 = new Account(0, account3);
      MobilePhone phoneStudent3 = new MobilePhone (phone3, 0 , accountStudent3);
      student3 = new Student(nameStudent3,phoneStudent3);
      StudentPrint(student3);

      studentCall(student1);
      studentCall(student2);
      studentCall(student3); 

      studentTopUp(student1);
      studentTopUp(student2);
      studentTopUp(student3);   
   
      studentCall(student1);
      studentCall(student2);
      studentCall(student3); 

      studentCall(student1);
      studentCall(student2);
      studentCall(student3); 

      System.out.println("Now let us discard a phone");
      System.out.println(student3);
      phone3 = "Amperium";  
      System.out.println("is buying a new phone " + phone3);  
      account3 = "Vaco"; 
      System.out.println("with account " + account3);
      accountStudent3 = new Account(0, account3);
      phoneStudent3 = new MobilePhone (phone3, 0 , accountStudent3);
      student3 = new Student(nameStudent3,phoneStudent3);
      StudentPrint(student3);

      studentTopUp(student3);   
      studentCall(student3); 
   }//main

   private static void studentCall(Student studentObject)
   {
      double seconds = Math.random() * 1000;
      int secondsInt = (int) seconds;
      System.out.println(studentObject.studentCall(secondsInt));  
      System.out.println("is making a call for desired " + secondsInt 
                         + " seconds");  
      StudentPrint(studentObject);
   }
 
   private static void studentTopUp(Student studentObject)
   {
      double topUp = Math.random() * 100;
      int topUpInt = (int) topUp;
      System.out.println(studentObject.topUpStudent(topUpInt));  
      System.out.println("is topping up by " + topUpInt);  
      StudentPrint(studentObject);
   }

   private static void StudentPrint(Student student)
   {   
   System.out.println("Result");
   System.out.println(student.toString());
   System.out.println(); 
   }
}//class
