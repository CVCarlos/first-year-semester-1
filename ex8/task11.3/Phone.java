/*
   This class create an object of each mobile phone
   that is buyed.
   Program made by Carlos Cerda Veloz, group W
*/
public class Phone
{
   private final String model;
   private int seconds;
   private final Account account;

   public MobilePhone (String modelPhone, int secondsPhone, 
                       Account accountPhone)
   {
      model = modelPhone;
      seconds = secondsPhone;
      account = accountPhone;
   }

   //Print the Mobile information
   public String toString()
   {
      return "Phone(" + model + "," + seconds + "," + account + ")"; 
   }

   //Add the seconds of the call to the mobile phone register
   public String secondsCheck(int secondsCall)
   {
      String result = account.SecondsCall(secondsCall);
      seconds += account.getSeconds();
      return result;
   }

   //Add the top up to the account balance
   public String topUpCheck(int topUp)
   {
      return account.topUpCheck(topUp);
   }

}//class

