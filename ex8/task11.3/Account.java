/*
   This class create an object of each account
   that is created.
   Program made by Carlos Cerda Veloz, group W
*/
public class Account
{
   //Balance of the account
   private int seconds, secondsLastValue;

   //Name of the account
   private final String account;

   //Line separator
   private static String NLS = System.getProperty("line.separator");

   //Constructor method
   public Account (int secondsAccount, String accountName)
   {
      seconds = secondsAccount;
      account = accountName;
   }

   //Print the Account information
   public String toString()
   {
      return "Account(" + account + "," + seconds + ")"; 
   }
    
   //Return the correct value for the  duration of the call
   public int getSeconds()
   {
      return secondsLastValue;
   }

   //Determines if the full duration of the call can be applied
   public String SecondsCall(int secondsCall)
   {
      if (seconds >= secondsCall)
      {
         seconds -= secondsCall;   
         secondsLastValue = secondsCall;      
         return "";   
      }
      else 
      {
         secondsLastValue = seconds;      
         seconds = 0;        
         return "This next call should be truncated to " + seconds + NLS;            
      }
   }

   //Top up the balance of the account
   public String topUpCheck(int topUp)
   {
      seconds = topUp * 100;   
      return "";        
   }
  
}//class

