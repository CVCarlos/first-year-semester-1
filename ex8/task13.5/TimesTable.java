/** 
   This program prints a Times Table
   message making use 
   of a GUI interface
   
   @author Carlos Cerda Veloz
*/

import java.util.Scanner;
import java.awt.Container;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class TimesTable extends JFrame
{
   private static
   Scanner inputScanner = new Scanner(System.in);

   /** 
      This method construct the TimesTable object. 
      For do that the int values are converted to String
      
      @param maximumNumber The maximum number to be used
      @param multiplier The number that is used as multiplier
   */
   public TimesTable(int maximumNumber, int multiplier)
   {
      setTitle("Times Table");
      Container contents = getContentPane();
      contents.setLayout(new GridLayout(0,5,50,20));
      for(int index=1; index <= maximumNumber; index++)
      {
         String indexString = Integer.toString(index);
         contents.add(new JLabel(indexString));
         contents.add(new JLabel("X"));
         String multiplierString = Integer.toString(multiplier);
         contents.add(new JLabel(multiplierString));
         contents.add(new JLabel("="));
         int result = index * multiplier;
         String resultString = Integer.toString(result);
         contents.add(new JLabel(resultString)); 
      } 

         setDefaultCloseOperation(EXIT_ON_CLOSE);
         pack();
   }

   /** 
      This method takes two integer
      commans lines arguments and 
      prints the table
   */
   public static void main(String[] args)
   {
      System.out.println("Input values for maximum number and multiplier");
      int maximumNumber = inputScanner.nextInt();
      int multiplier = inputScanner.nextInt();
      TimesTable table = new TimesTable(maximumNumber, multiplier);
      table.setVisible(true);
   }   
}//class

