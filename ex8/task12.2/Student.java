/**
   This class represents a Student and contains different
   ways of manipulate this. 

   @author Carlos Cerda Veloz
*/
public class Student
{ 
   //Name of the student
   private final String name;

   //Information about the phone and account
   private MobilePhone phone; 

   //Line separator
   private static String NLS = System.getProperty("line.separator");
   
/**
   This is a constructor method for a Student
  
   @param nameStudent The name of the student
   @param phoneStudent The model of the phone for the student
*/
   public Student (String nameStudent, MobilePhone phoneStudent)
   {
      name = nameStudent;
      phone = phoneStudent;
   }

/**
   This method prints the student information
*/
   public String toString()
   {
      return "Student("+ name+ "," + phone + ")";
   }

/**
   This method call a method in phone to determine the maximum possible
   duration of the call
   
   @param seconds The seconds that the call is intended to use
   @return The method returns an error message if the student has not a phone
           or a message with the results of the call if the student has a phone
*/
   public String studentCall(int seconds)
   {
      if (phone == null)
      return "This next call has no effect, as has no phone!" + NLS 
             + toString();
      else
      return phone.secondsCheck(seconds) + toString();
   }

/**
   This method call a method in phone to top up the balance
   
   @param topUp The amount of pounds that are going to be added
   @return The method returns an error message if the student has not a phone
           or a message with the results of the top up if the student has
           a phone
*/
   public String topUpStudent(int topUp)
   {
      if (phone == null)
      return "This next top up has no effect, as has no phone!" + NLS 
             + toString();
      else
      return toString() + phone.topUpCheck(topUp);
   }   
}//class
