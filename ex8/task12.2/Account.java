/**
   This class represents an Account and contains different
   ways of manipulate this. 

   @author Carlos Cerda Veloz
*/
public class Account
{
   //Balance of the account
   private int seconds, secondsLastValue;

   //Name of the account
   private final String account;

   //Line separator
   private static String NLS = System.getProperty("line.separator");

/**
   This is a constructor method for an object Account 
   @param secondsAccount The current balance of the account
   @param accountName The name of the account
*/
   public Account (int secondsAccount, String accountName)
   {
      seconds = secondsAccount;
      account = accountName;
   }

/**
   This method prints the information about the account
*/
   public String toString()
   {
      return "Account(" + account + "," + seconds + ")"; 
   }
    
/**
   This method is used to return a number of seconds
   which represent the total possible duration of a call  
*/
   public int getSeconds()
   {
      return secondsLastValue;
   }

/**
   This method is used to determine the maximum possible duration
   of a call and substract this from the balance of the account
  
   @param secondsCall The seconds that the call is intended to use
   @return Empty if the full time of the call is possible, or a message 
           if the duration needed be truncated.
*/
   public String SecondsCall(int secondsCall)
   {
      if (seconds >= secondsCall)
      {
         seconds -= secondsCall;   
         secondsLastValue = secondsCall;      
         return "";   
      }
      else 
      {
         secondsLastValue = seconds;      
         seconds = 0;        
         return "This next call should be truncated to " + seconds + NLS;            
      }
   }

/**
   This method is used to top up the balance of the account. Transform
   from pounds to seconds.
  
   @param topUp The total pounds that are going to be added
   @return Empty
*/
   public String topUpCheck(int topUp)
   {
      seconds = topUp * 100;   
      return "";        
   }
  
}//class

