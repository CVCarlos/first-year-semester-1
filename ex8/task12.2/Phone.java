/**
   This class represents a Mobile Phone and contains different
   ways of manipulate this. 

   @author Carlos Cerda Veloz
*/
public class Phone
{
   private final String model;
   private int seconds;
   private final Account account;

/**
   This is a constructor method for a Mobile Phone
  
   @param modelPhone The model of the phone
   @param secondsPhone The seconds that the phone have in its register
   @param accountPhone The name of the account that the phone is using 
*/
   public MobilePhone (String modelPhone, int secondsPhone, 
                       Account accountPhone)
   {
      model = modelPhone;
      seconds = secondsPhone;
      account = accountPhone;
   }

/**
   This method prints the information about the phone 
*/
   public String toString()
   {
      return "Phone(" + model + "," + seconds + "," + account + ")"; 
   }

/**
   This method add seconds to the register of the mobile phone
   @param secondsCall The seconds that the call is intended to use
   @return Returns a result which can be empty or a message about
           the truncation of the duration of the call
*/
   public String secondsCheck(int secondsCall)
   {
      String result = account.SecondsCall(secondsCall);
      seconds += account.getSeconds();
      return result;
   }

/**
   This method call to a method in account. It is used to top up the balance
   @param topUp The amount of pounds that are going to be added
*/
   public String topUpCheck(int topUp)
   {
      return account.topUpCheck(topUp);
   }

}//class

